module.exports = function (sequelize, Sequelize) {
    var aux = require('../models/rol');
    var Rol = new aux(sequelize, Sequelize);
    var Persona = sequelize.define('persona', {
       id: {
           autoIncrement: true,
           primaryKey: true,
           type: Sequelize.INTEGER
       },
       apellidos: {
           type: Sequelize.STRING (60),
           notEmpty: true
       },
       nombres: {
           type: Sequelize.STRING (60)
       },
       correo: {
           type: Sequelize.STRING (60)
       },
       external_id: {
           type: Sequelize.UUID
       }
    });
    Persona.associate = function (models) {
        
        models.persona.hasOne(models.cuenta, {            
            foreignKey: 'id_persona'
        });
        models.persona.hasMany(models.pelicula, {            
            foreignKey: 'id_persona'
        });
    };    
    Persona.belongsTo(Rol, {foreignKey: 'id_rol',constraints: false});
    return Persona;
};