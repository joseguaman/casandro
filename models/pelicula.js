
module.exports = function (sequelize, Sequelize) {
    var Pelicula = sequelize.define('pelicula', {
       id: {
           autoIncrement: true,
           primaryKey: true,
           type: Sequelize.INTEGER
       },
       titulo: {
           type: Sequelize.STRING (160),
           notEmpty: true
       },
       actores: {
           type: Sequelize.STRING (255)
       },
       director: {
           type: Sequelize.STRING (150)           
       },
       genero: {
           type: Sequelize.STRING (150)           
       },
       portada: {
           type: Sequelize.STRING (250)           
       },
       external_id: {
           type: Sequelize.UUID
       },
       codigo: {
           type: Sequelize.STRING (20)
       },
       anio: {
           type: Sequelize.INTEGER
       }
    });
    
    return Pelicula;
};




