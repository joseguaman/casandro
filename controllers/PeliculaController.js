'use strict';
var models = require('./../models');
class PeliculaController {

    verListaWs(req, res, next) {
        //this.isLoggedIn(req, res, next);

        res.render('template/templateI', {title: 'Listado de peliculas por servicio web',
            login: true,
            fragmento: '../fragmentos/admin/peliculasws/listar',
            rol: req.user.rol});
    }
    
    verRegistro(req, res, next) {
        //this.isLoggedIn(req, res, next);

        res.render('template/templateI', {title: 'Listado de peliculas por servicio web',
            login: true,
            fragmento: '../fragmentos/admin/peliculasws/registro',
            rol: req.user.rol});
    }

    guardar(req, res, next) {
        var Persona = models.persona;        
        var id = req.user.id_persona;
        console.log(req.user.id_persona+" *************8");
        Persona.findOne({where: {external_id: id}}).then(function (persona) {
            if (persona) {
                var data = {
                    titulo: req.body.txt_titulo,
                    actores: req.body.txt_actor,
                    id_persona: persona.id
                };
                var Pelicula = models.pelicula;
                Pelicula.create(data).then(function (newPelicula, created){
                    if(newPelicula) {
                        res.redirect('/admin/listado/ws');
                    } else {
                        res.redirect('/admin/pelicula/registro');
                    }
                });
            } else {
                res.redirect('/admin/pelicula/registro');
            }
        });
        
    }

    isLoggedIn(req, res, next) {
        if (!req.isAuthenticated())
            res.redirect('/inicio');
    }
}
module.exports = PeliculaController;
