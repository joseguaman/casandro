var express = require('express');
var router = express.Router();
var autentification = require('../controllers/AutentificacionControllers');
var authController = new autentification();
var passport   = require('passport');
var listws = require('../controllers/PeliculaController');
var peliculaController = new listws();

/* GET home page. */
router.get('/', function (req, res, next) {
    //console.log(req.isAuthenticated()+" hola ***** "+JSON.stringify(req.user));    
    if(req.isAuthenticated()) {
        res.render('template/index', {title: 'Principal', login: req.isAuthenticated(), rol: req.user.rol});
    } else {
        res.render('template/index', {title: 'Principal', login: req.isAuthenticated(), rol: ''});
    }
    
});
//login
router.get('/inicio', authController.signin);
router.post('/login', passport.authenticate('local-signin',  { successRedirect: '/',
                                                 failureRedirect: '/inicio'}
                                                    ));
                                            
router.get('/cerrar', authController.logout);
                                            
//registro
router.get('/registro', authController.signup);
router.post('/registro/save', passport.authenticate('local-signup', {successRedirect: '/inicio',
    failureRedirect: '/registro'}
));
//peliculas
router.get('/admin/listado/ws', peliculaController.verListaWs);
router.get('/admin/pelicula/registro', peliculaController.verRegistro);
router.post('/admin/pelicula/guardar', peliculaController.guardar);

function cargarVista(req, res, fragmento) {
    var login = (req.session.user != undefined);
    if (login == true) {
        res.render('template/index', {title: 'Principal', login: login,
            fragmento: fragmento, usuario: req.session.user});
    } else {
        res.render('template/index', {title: 'Principal', login: login});
    }
}



module.exports = router;
